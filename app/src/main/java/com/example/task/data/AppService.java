package com.example.task.data;

import com.example.task.data.remote.model.PlacesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AppService {

    String BASE_URL = "https://api.foursquare.com/v2/venues/";

    @GET("search?ll=40.7484,-73.9857&oauth_token=NPKYZ3WZ1VYMNAZ2FLX1WLECAWSMUVOQZOIDBN53F3LVZBPQ&v=20180616")
    Call<PlacesResponse> getPlaces();

}
