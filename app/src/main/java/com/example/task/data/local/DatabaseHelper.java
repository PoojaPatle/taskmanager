package com.example.task.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.task.data.local.model.PlacesModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "places_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(PlacesModel.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + PlacesModel.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void insertPlace(PlacesModel placesModel) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // timestamp` will be inserted automatically.
        values.put(PlacesModel.COLUMN_ID, placesModel.getId());
        values.put(PlacesModel.COLUMN_NAME, placesModel.getName());
        values.put(PlacesModel.COLUMN_CONTACT, placesModel.getContact());
        values.put(PlacesModel.COLUMN_DISTANCE, placesModel.getDistance());
        values.put(PlacesModel.COLUMN_ADDRESS, placesModel.getAddress());
        values.put(PlacesModel.COLUMN_FAVORITE, placesModel.getFavorite());

        // insert row
        db.insert(PlacesModel.TABLE_NAME, null, values);

        // close db connection
        db.close();

    }

    public PlacesModel getPlace(String id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(PlacesModel.TABLE_NAME,
                new String[]{PlacesModel.COLUMN_ID, PlacesModel.COLUMN_NAME, PlacesModel.COLUMN_CONTACT,
                        PlacesModel.COLUMN_DISTANCE, PlacesModel.COLUMN_ADDRESS, PlacesModel.COLUMN_FAVORITE},
                PlacesModel.COLUMN_ID + "=?",
                new String[]{id}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare note object
        PlacesModel model = new PlacesModel(
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_CONTACT)),
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_DISTANCE)),
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_ADDRESS)),
                cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_FAVORITE)));

        // close the db connection
        cursor.close();

        return model;
    }

    public List<PlacesModel> getAllPlaces() {
        List<PlacesModel> places = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + PlacesModel.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PlacesModel model = new PlacesModel();
                model.setId(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_ID)));
                model.setName(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_NAME)));
                model.setContact(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_CONTACT)));
                model.setDistance(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_DISTANCE)));
                model.setAddress(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_ADDRESS)));
                model.setFavorite(cursor.getString(cursor.getColumnIndex(PlacesModel.COLUMN_FAVORITE)));

                places.add(model);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return places list
        return places;
    }

    public int getPlacesCount() {
        String countQuery = "SELECT  * FROM " + PlacesModel.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();
        // return count
        return count;
    }

    public int updatePlace(PlacesModel model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PlacesModel.COLUMN_FAVORITE, model.getFavorite());

        // updating row
        return db.update(PlacesModel.TABLE_NAME, values, PlacesModel.COLUMN_ID + " = ?",
                new String[]{String.valueOf(model.getId())});
    }

    public void deletePlace(PlacesModel note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PlacesModel.TABLE_NAME, PlacesModel.COLUMN_ID + " = ?",
                new String[]{String.valueOf(note.getId())});
        db.close();
    }

}
