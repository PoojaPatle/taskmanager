package com.example.task.data.local.model;

public class PlacesModel {

    public static final String TABLE_NAME = "place";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CONTACT = "contact";
    public static final String COLUMN_DISTANCE = "distance";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_FAVORITE = "favorite";

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " TEXT PRIMARY KEY,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_CONTACT + " TEXT,"
                    + COLUMN_DISTANCE + " TEXT,"
                    + COLUMN_ADDRESS + " TEXT,"
                    + COLUMN_FAVORITE + " TEXT"
                    + ")";

    String id;
    String name;
    String contact;
    String distance;
    String address;
    String favorite;

    public PlacesModel() {

    }

    public PlacesModel(String id, String name,
                       String contact, String distance,
                       String address, String favorite) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.distance = distance;
        this.address = address;
        this.favorite = favorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }
}
