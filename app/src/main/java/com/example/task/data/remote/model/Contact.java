package com.example.task.data.remote.model;

import com.google.gson.annotations.SerializedName;

public class Contact{

	@SerializedName("phone")
	private String phone;

	@SerializedName("formattedPhone")
	private String formattedPhone;

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setFormattedPhone(String formattedPhone){
		this.formattedPhone = formattedPhone;
	}

	public String getFormattedPhone(){
		return formattedPhone;
	}
}