package com.example.task.data.remote.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("venues")
	private List<Venues> venues;

	public void setVenues(List<Venues> venues){
		this.venues = venues;
	}

	public List<Venues> getVenues(){
		return venues;
	}
}