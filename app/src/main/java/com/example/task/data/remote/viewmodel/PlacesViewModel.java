package com.example.task.data.remote.viewmodel;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.task.data.AppService;
import com.example.task.data.remote.model.PlacesResponse;
import com.example.task.data.remote.model.Venues;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlacesViewModel extends ViewModel {

    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<Venues>> liveData;

    //we will call this method to get the data
    public LiveData<List<Venues>> getPlacesLists() {
        //if the list is null
        if (liveData == null) {
            liveData = new MutableLiveData<List<Venues>>();
            //we will load it asynchronously from server in this method
            loadPlaces();
        }

        //finally we will return the list
        return liveData;
    }

    //This method is using Retrofit to get the JSON data from URL
    private void loadPlaces() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppService.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AppService api = retrofit.create(AppService.class);

        Call<PlacesResponse> call = api.getPlaces();

        call.enqueue(new Callback<PlacesResponse>() {
            @Override
            public void onResponse(Call<PlacesResponse> call, Response<PlacesResponse> response) {
                liveData.postValue(response.body().getResponse().getVenues());
            }

            @Override
            public void onFailure(Call<PlacesResponse> call, Throwable t) {
                Log.e("TAG", "onResponse:error " + t.getLocalizedMessage());
            }
        });

    }
}
