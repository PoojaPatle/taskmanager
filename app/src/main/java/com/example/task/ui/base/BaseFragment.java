package com.example.task.ui.base;

import android.app.ProgressDialog;

import androidx.fragment.app.Fragment;

import com.example.task.utils.StringUtils;

public class BaseFragment extends Fragment {

    protected ProgressDialog mProgressDialog;


    public void showLoading() {

        getActivity().runOnUiThread(() -> {
            hideLoading();
            mProgressDialog = StringUtils.showLoadingDialog(getActivity());
        });

    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }


}
