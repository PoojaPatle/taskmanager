package com.example.task.ui.listeners;

public interface OnItemClickListener {
    void onClickFavorites(int position);
}
