package com.example.task.ui.matches;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.data.local.model.PlacesModel;
import com.example.task.databinding.CellItemListsBinding;
import com.example.task.ui.listeners.OnItemClickListener;
import com.example.task.ui.base.BaseViewHolder;

import java.util.List;

public class AllMatchedAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    List<PlacesModel> mVenues;
    Context mContext;
    public OnItemClickListener clickListener;


    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public AllMatchedAdapter(List<PlacesModel> mVenues, Context mContext) {
        this.mVenues = mVenues;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new AllMatchViewHolder(CellItemListsBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false
        ));
    }


    public void updateFav(String value, int position) {
        if (value.equals("1")) {
            mVenues.get(position).setFavorite(value);
        } else {
            mVenues.get(position).setFavorite(value);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        return mVenues.size();
    }

    public class AllMatchViewHolder extends BaseViewHolder {

        public CellItemListsBinding binding;
        AllMatchesViewModel viewModel;

        public AllMatchViewHolder(@NonNull CellItemListsBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;

        }

        @Override
        public void onBind(int position) {
            PlacesModel venues = mVenues.get(position);
            viewModel = new AllMatchesViewModel(venues);
            binding.setModel(viewModel);
            binding.executePendingBindings();

            if (clickListener != null) {

                binding.starIcon.setOnClickListener(v -> {
                    clickListener.onClickFavorites(position);
                });
            }
        }
    }
}
