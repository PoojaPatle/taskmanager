package com.example.task.ui.matches;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.example.task.data.local.DatabaseHelper;
import com.example.task.data.local.model.PlacesModel;
import com.example.task.data.remote.model.Venues;
import com.example.task.data.remote.viewmodel.PlacesViewModel;
import com.example.task.databinding.FragmentAllMatchesBinding;
import com.example.task.ui.base.BaseFragment;
import com.example.task.ui.listeners.OnItemClickListener;
import com.example.task.utils.StringUtils;

import java.util.List;

public class AllMatchesFragment extends BaseFragment implements OnItemClickListener {

    private PlacesViewModel viewModel;
    private FragmentAllMatchesBinding binding;
    private DatabaseHelper db;
    AllMatchedAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(PlacesViewModel.class);
        binding = FragmentAllMatchesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        db = new DatabaseHelper(requireActivity());

        showLoading();
        viewModel.getPlacesLists().observe(requireActivity(), result -> {
            hideLoading();
            fetchData(result);
        });

        return root;
    }

    private void fetchData(List<Venues> venues) {
        try {
            if (db.getPlacesCount() == 0) {
                for (Venues venu : venues) {
                    db.insertPlace(new PlacesModel(
                            venu.getId(),
                            venu.getName(),
                            venu.getContact().getPhone(),
                            String.valueOf(venu.getLocation().getDistance()),
                            StringUtils.getAddress(venu.getLocation().getFormattedAddress()),
                            "0"
                    ));
                }
            }

            adapter = new AllMatchedAdapter(db.getAllPlaces(), requireActivity());
            adapter.setClickListener(this);
            binding.setMAdapter(adapter);

        } catch (Exception exception) {
            Log.e("TAG", "fetchData: error");
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /*List item click method*/
    @Override
    public void onClickFavorites(int position) {
        try {

            PlacesModel model = db.getAllPlaces().get(position);
            if (model.getFavorite().equals("0")) {
                model.setFavorite("1");
                db.updatePlace(model); // update value in database
                adapter.updateFav("1", position); //change icon on lists side
            } else {
                model.setFavorite("0");
                db.updatePlace(model);
                adapter.updateFav("0", position);
            }

        } catch (Exception exception) {
            Log.d("TAG", "onClickFavorites: error");
        }
    }
}