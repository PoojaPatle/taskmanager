package com.example.task.ui.matches;

import androidx.databinding.ObservableField;

import com.example.task.data.local.model.PlacesModel;

public class AllMatchesViewModel {

    public final ObservableField<String> name;
    public final ObservableField<String> address;
    public final ObservableField<String> contact;
    public final ObservableField<String> distance;
    public final ObservableField<String> isFavourite;

    public AllMatchesViewModel(PlacesModel venues) {

        name = new ObservableField<>(venues.getName());
        address = new ObservableField<>(venues.getAddress());
        contact = new ObservableField<>(venues.getContact() != null ? venues.getContact() : "Not available");
        distance = new ObservableField<>(venues.getDistance() + " Km");
        isFavourite = new ObservableField<>(venues.getFavorite());

    }

}