package com.example.task.ui.saved;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.data.local.model.PlacesModel;
import com.example.task.databinding.CellItemListsBinding;
import com.example.task.ui.listeners.OnItemClickListener;
import com.example.task.ui.matches.AllMatchesViewModel;
import com.example.task.ui.base.BaseViewHolder;

import java.util.List;

public class SavePlacesAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    List<PlacesModel> mVenues;
    Context mContext;
    public OnItemClickListener clickListener;

    public SavePlacesAdapter(List<PlacesModel> mVenues, Context mContext) {
        this.mVenues = mVenues;
        this.mContext = mContext;
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SaveViewHolder(CellItemListsBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    public void updateFav(int position) {
        mVenues.remove(getPlace(position));
        notifyDataSetChanged();
    }

    public PlacesModel getPlace(int pos) {
        return mVenues.get(pos);
    }

    @Override
    public int getItemCount() {
        return mVenues.size();
    }

    private class SaveViewHolder extends BaseViewHolder {

        public CellItemListsBinding binding;
        AllMatchesViewModel viewModel;

        public SaveViewHolder(CellItemListsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            PlacesModel venues = mVenues.get(position);
            viewModel = new AllMatchesViewModel(venues);
            binding.setModel(viewModel);
            binding.executePendingBindings();

            if (clickListener != null) {

                binding.starIcon.setOnClickListener(v -> {
                    clickListener.onClickFavorites(position);
                });
            }


        }
    }
}
