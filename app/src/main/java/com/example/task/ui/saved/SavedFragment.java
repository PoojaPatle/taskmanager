package com.example.task.ui.saved;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.task.data.local.DatabaseHelper;
import com.example.task.data.local.model.PlacesModel;
import com.example.task.databinding.FragmentSavedBinding;
import com.example.task.ui.listeners.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class SavedFragment extends Fragment implements OnItemClickListener {

    private FragmentSavedBinding binding;
    private DatabaseHelper db;
    SavePlacesAdapter mAdapter;

    List<PlacesModel> models = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSavedBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        db = new DatabaseHelper(requireActivity());

        try {
            if (db.getAllPlaces() != null) {
                for (PlacesModel item : db.getAllPlaces()) {
                    if (!item.getFavorite().equals("0")) {
                        models.add(item);
                    }
                }
            }
        } catch (Exception exception) {

        }

        mAdapter = new SavePlacesAdapter(models, requireActivity());
        mAdapter.setClickListener(this);
        binding.setMAdapter(mAdapter);

        togglePlaces();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onClickFavorites(int position) {
        try {
            PlacesModel model = models.get(position);
            if (model.getFavorite().equals("1")) {
                model.setFavorite("0");
                db.updatePlace(model);
                mAdapter.updateFav(position);
                togglePlaces();
            }
        } catch (Exception exception) {
            Log.d("TAG", "onClickFavorites: error");
        }
    }

    /**
     * Toggling list and empty places view
     */
    private void togglePlaces() {
        if (models.size() > 0) {
            binding.txtNoDataFound.setVisibility(View.GONE);
        } else {
            binding.txtNoDataFound.setVisibility(View.VISIBLE);
        }
    }
}