package com.example.task.utils;

import android.content.Context;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.example.task.R;

public final class BindingUtils {

    private BindingUtils() {
        // This class is not publicly instantiable
    }


    @BindingAdapter("isFavourite")
    public static void setFavourite(ImageView imageView, String url) {
        Context context = imageView.getContext();
        if (url.equals("1"))
            imageView.setImageResource(R.drawable.ic_star);
        else {
            imageView.setImageResource(R.drawable.ic_star_outline);

        }
    }
}
